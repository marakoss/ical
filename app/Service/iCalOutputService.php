<?php

namespace App\Service;

use \Eluceo\iCal\Component\Calendar;
use \Eluceo\iCal\Component\Event;

/**
 * Output events in iCal format
 *
 * @package calendar
 */
class iCalOutputService {

	/** @var object[] */
	private $events;

	/**
	 * @param object[] $events
	*/
	public function __construct($events = []) {
		$this->setEvents($events);
	}

	/**
	 * @param object[] $events
	 */
	private function setEvents($events) {
		$this->events = $events;
	}

	/**
	 * @param object $event
	 * @return \Eluceo\iCal\Component\Event
	 */
	private function createCalEvent($event) {

		$vEvent = new Event();
		$vEvent
			->setDtStart(new \DateTime($event->datefrom))
			->setDtEnd(new \DateTime($event->dateto))
			->setSummary($event->message);
		return $vEvent;
	}

	/**
	 * Outputs iCal format
	 *
	 * Note: Controller should set response headers
	 * header('Content-Type: text/calendar; charset=utf-8');
	 * header('Content-Disposition: attachment; filename="cal.ics"');
	 *
	 * @return string
	 */
	public function getOutput() {

		$vCalendar =  new Calendar('events');
		foreach ($this->events as $event) {
			$vCalendar->addComponent($this->createCalEvent($event));
		}

		return $vCalendar->render();

	}

}